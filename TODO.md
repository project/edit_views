1. Figure out a way to *start* in-place editing inside a View — on a per-entity basis, just like elsewhere.
2. Figure out how to make Views use the changed entity in TempStore. See edit_views_edit_render_field().
