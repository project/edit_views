# Description

In-place editing of entity fields in Views.


# Installation

This is an **experimental** project! Supporting in-place editing of entity fields in Views is non-trivial!


# FAQ

## I want to disable in-place editing of fields in a View.
It's possible to disable in-place editing in Views *completely* by implementing `hook_module_implements_alter()`, and removing `edit_views_preprocess_views_view_field()` from the list of implementations. Currently, it's not yet possible to disable in-place editing in only a specific View.
